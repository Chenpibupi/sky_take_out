package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Component
@ConfigurationProperties(prefix = "sky.qiniu")
@Data
public class QiniuProperties {
    private String accessKey;
    private String secretKey;
    private String bucket;
    private String localFilePath;
    private String domainOfBucket;
}

