package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 套餐管理
 */
@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
@Api(tags = "套餐管理") // 添加@Api注解，定义套餐管理的Swagger文档分组
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @PostMapping
    @ApiOperation("新增套餐") // 使用@ApiOperation注解添加操作描述
    @CacheEvict(cacheNames = "setmealCache",key = "#setmealDTO.categoryId")
    public Result save(@RequestBody SetmealDTO setmealDTO) {
        log.info("接收到的套餐信息：{}", setmealDTO.toString());
        setmealService.saveWithDishs(setmealDTO);
        return Result.success();
    }

    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result<PageResult> page(SetmealPageQueryDTO setmealPageQueryDTO) {
        log.info("套餐分页查询：{}", setmealPageQueryDTO);
        PageResult pageInfo = setmealService.getSetmealsByPage(setmealPageQueryDTO);
        return Result.success(pageInfo);
    }

    @DeleteMapping
    @ApiOperation("删除套餐")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    public Result deleteByIds(@RequestParam List<Long> ids) {
        log.info("要删除的ids为：{}", ids);
        setmealService.deleteBatch(ids);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据套餐ID查询套餐")
    public Result<SetmealVO> getById(@PathVariable Long id) {
        log.info("接收到的套餐id为：{}", id);
        SetmealVO setmealVO = setmealService.getByIdWithDishs(id);
        return Result.success(setmealVO);
    }

    @PutMapping
    @ApiOperation("更新套餐信息")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    public Result update(@RequestBody SetmealDTO setmealDTO) {
        log.info("接收到的待修改套餐信息：{}", setmealDTO.toString());
        setmealService.updateWithDishs(setmealDTO);
        return Result.success();
    }

    @PostMapping("/status/{status}")
    @ApiOperation("启用/禁用套餐")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    // todo 验证使用精准套餐清理
//    @CacheEvict(cacheNames = "setmealCache",key = "#id")
    public Result updateStatus(@PathVariable Integer status, @RequestParam Long id) {
        log.info("启用/禁用套餐：{},{}", status, id);
        setmealService.updateStatus(status, id);
        return Result.success();
    }
}
