package com.sky.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 菜品口味
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class DishFlavor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "口味ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "菜品ID", example = "1")
    private Long dishId;

    @ApiModelProperty(value = "口味名称", example = "辣味")
    private String name;

    @ApiModelProperty(value = "口味数据", example = "[1,2,3]")
    private String value;

}
